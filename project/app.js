var querydb = require("./query.js");
const express = require('express')
const app = express()
const port = 3000

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database: 'devops'
});

app.get('/', async (req, res) => {

  connection.connect(function(err) {
    if (err) {
      res.send('Failed to connect.');
    } else {
      res.send('Connection successful!');
    }
  });

  // const results = await querydb(connection, 'SELECT * FROM test').catch(console.log);
  connection.end();
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
