# Configure AWS
## Setup Ansible on AWS

**RHEL 8:**
```sh
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
sudo dnf config-manager --set-enabled codeready-builder-for-rhel-8-rhui-rpms
sudo dnf install ansible -y
```

## Expose port

The port `3000` must be exposed.

## Retrieve SSH key

Install the SSH private key in a PEM file inside the `pem` folder.
Update the `inventory.yml` file, and set your servers.

# Deployment
## Ansible galaxy 

Install dependencies:
```
ansible-galaxy install -r requirements.yml
```

## Execute playbook

```sh
ansible-playbook playbook.yml -i inventory.yml
```

or 

```sh
./deploy.sh
```
